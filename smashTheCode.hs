{-# LANGUAGE BangPatterns, MagicHash, UnboxedTuples #-}

module Main where

import Data.Tree
import Data.Int
--import GHC.Base
import qualified Data.Text as T
import Data.STRef
import System.IO
import Data.Char
import Control.Monad
import Control.Monad.ST
import System.Random
import Data.List
import qualified Data.Sequence as S
import Debug.Trace
import Data.Array.ST
import qualified Data.Foldable as F
import qualified Data.Map as M
import qualified Data.Array.ST as ST
import qualified Data.Array.Unboxed as A
import qualified Data.Traversable
import qualified Data.Array.MArray
--import GHC.Exts

--import qualified Data.Vector as V

mctest = getBestMoveMCTS gameState4

main :: IO ()
main = do
    hSetBuffering stdout NoBuffering -- DO NOT REMOVE

    -- Auto-generated code below aims at helping you parse
    -- the standard input according to the problem statement.
    --putStrLn $ sexyPrintGameStates $ findChildren gs3

    --let (s,mgs) = getBestMaxmaxCleverEval gameState4 3
    --putStrLn $ pr $ (fromIntegral s, mgs)
    --let res = getBestMoveMCTS gameState4
    --putStrLn $ pr $ res

    -- return ()
    loop


getActivated = False

myTrace activated msg code =
    if (activated) then
        trace msg code
    else
        code


-- data types

type Puyo = (Int8, Int8) --color1, color2

type Pos = (Int8, Int8)  --x,y

px pos = fst pos
py pos = snd pos

data Move = Move { --liste de deux puyos positionnés
    mvFstPuyo   :: (Pos, Int8),
    mvSndPuyo   :: (Pos, Int8),
    mvRot       :: Int8,
    mvCol       :: Int8
} deriving (Show)

data GameState = GameState {
    gsBoard           :: Board,
    gsNextPuyos       :: [Puyo],
    gsScore           :: Int,
    gsStaticScore     :: Int
}

data Tree = Tree {
    treeGS       :: GameState,
    treeChildren :: [GameState]
}

type Cell = Int8

invalidCell :: Cell
invalidCell = (-2)

type InnerMap = A.UArray (Int8,Int8) Cell

data Board = Board !InnerMap
    --deriving (Show, Read)


--------------------------
--        MCTS          --
--------------------------
type SimulationValue = Int

data GameNode = GameNode {
    gnValue      :: Int,
    gnVC         :: Int, --visit count
    gnState      :: (Move, GameState),
    gnChildren   :: [GameNode],
    gnDeepth       :: Int
}

getBestMoveMCTS :: GameState -> (Double, (Move, GameState))
getBestMoveMCTS gs =
    let (finalGameNode, finalEvaluation) = foldl executeMCTS (root,0) [0..200] in
    let bestchild = findBestChild finalGameNode in
    (evalNode bestchild, gnState bestchild)
    where
        root = createNode 1 (defaultMove 0, gs)
        executeMCTS (node, _) x = explore node
        findBestChild node = maximumBy compareNodes (gnChildren node)
        compareNodes n1 n2 =
            compare (evalNode n1) (evalNode n2)
        evalNode n = (fromIntegral $ gnValue n) / (fromIntegral $ gnVC n)

pr :: (Double, (Move, GameState)) -> String
pr (score, (move, gs)) = "score " ++ show (score) ++ (show move)

explore :: GameNode -> (GameNode, SimulationValue)
explore node
    | (length $ gnChildren $ node) == 0 = expandAndSimulate --trace ("expand ") (expandAndSimulate)
    | isTerminal node                   = backPropagate node --trace ("backPropagate ") (backPropagate node)
    | otherwise                         = exploration --trace ("exploration ") (exploration)
  where
    expandAndSimulate =
        let (expandedNode, simValue) = expand node  in
        (expandedNode, simValue)
    exploration =
        let numvisitcurrentNode = gnVC node in
        let !(bestNode, otherNodes) = extractMaximum (gnChildren node) numvisitcurrentNode in
        let !(exploredNode, simValue) = explore bestNode  in
        let newNode = node {
                        gnValue    = (gnValue node) + simValue,
                        gnVC       = (gnVC node) + 1,
                        gnChildren = exploredNode:otherNodes } in
        (newNode, simValue)

createNode :: Int -> (Move,GameState) -> GameNode
createNode deepth moveGS@(m,gs) =
    GameNode {
        gnValue    = staticEvalGS gs,
        gnVC       = 1,
        gnState    = moveGS,
        gnChildren = [],
        gnDeepth   = deepth
    }

extractMaximum :: [GameNode] -> Int -> (GameNode, [GameNode])
extractMaximum [] t = error "extractMaximum fils vides"
extractMaximum (n:ns) t =
    foldl (getMax) (n,[]) ns
  where
     getMax (bestNode, nodes) node =
         if (isBest t bestNode node) then
             (bestNode, node:nodes)
         else
             (node, bestNode:nodes)

expand :: GameNode -> (GameNode, SimulationValue)
expand node =
    (expandedNode, simulate expandedNode)
  where
    expandedNode = node { gnValue = simulate $ node, gnVC = 1, gnChildren = childrenNodes}
    state = snd $ gnState node
    childrenStates = findChildren $ snd $ gnState node
    childrenNodes = map (createNode ((gnDeepth node) + 1)) childrenStates


--true si n1 > n2, t = nombre de visites du noeud père de n1 et n2
isBest :: Int -> GameNode -> GameNode -> Bool
isBest t n1 n2 =
    ucb1 > ucb2
  where
    ucb1 = ucb n1 t
    ucb2 = ucb n2 t

ucb :: GameNode -> Int -> Double
ucb node t =
    let wi = fromIntegral $ gnValue node in
    let ni = fromIntegral $ gnVC node in
    let t' = fromIntegral t in
    let deepth = gnDeepth node in
    let c = (20 + (6 ^ deepth)) in
    if (ni == 0) then
        error "ni == 0"
    else
        --(trace ("mean " ++ (show (wi / ni))) (wi / ni)) + (trace ("explor " ++ (show (c * sqrt ((log t') / ni)))) (c * sqrt ((log t') / ni)))
        (wi / ni) + c * sqrt ((log t') / ni)


simulate :: GameNode -> SimulationValue
simulate node = max (-200) (staticEvalGS $ snd $ gnState node)

backPropagate :: GameNode -> (GameNode, SimulationValue)
backPropagate node =
    let simValue = simulate node in
    (node {
        gnValue = (gnValue node) + simValue,
        gnVC = (gnVC node) + 1 }, simValue)

{-# INLINE mctsMaxBound #-}
mctsMaxBound :: SimulationValue
mctsMaxBound = 10000

{-# INLINE mctsMinBound #-}
mctsMinBound :: SimulationValue
mctsMinBound = -199

isTerminal :: GameNode -> Bool
isTerminal node =
    (currentScore > mctsMaxBound) || (currentScore < mctsMinBound) || (null nextpuyos)
    where
        currentScore = staticEvalGS $ snd $ gnState $ node
        nextpuyos = gsNextPuyos $ snd $ gnState $ node


-------------
-- CellsGroup
-------------

data CellsGroup = CellsGroup {
    cgColorCells :: S.Seq (Pos, Cell),
    cgBLockCells :: S.Seq (Pos, Cell)
} deriving (Show)

cgCreateWithOneBlock :: (Pos, Cell) -> CellsGroup
cgCreateWithOneBlock block = CellsGroup (S.empty) (S.singleton block)

cgCreateWithOneColor :: (Pos, Cell) -> CellsGroup
cgCreateWithOneColor color = CellsGroup (S.singleton color) (S.empty)

cgEmpty :: CellsGroup
cgEmpty = CellsGroup (S.empty) (S.empty)

cgAddColorCell :: CellsGroup -> Pos -> Cell -> CellsGroup
cgAddColorCell cellsGroup pos cell =
    let colors = cgColorCells cellsGroup in
    let blocks = cgBLockCells cellsGroup in
    CellsGroup ((pos, cell) S.<| colors) blocks

cgAddBlockCell :: CellsGroup -> Pos -> Cell -> CellsGroup
cgAddBlockCell cellsGroup pos cell =
    let colors = cgColorCells cellsGroup in
    let blocks = cgBLockCells cellsGroup in
    CellsGroup colors ((pos, cell) S.<| blocks)

cgAddCell :: CellsGroup -> Pos -> Cell -> CellsGroup
cgAddCell cellsGroup pos cell =
    if (isColorCell cell) then
        cgAddColorCell cellsGroup pos cell
    else
        cgAddBlockCell cellsGroup pos cell

{-# INLINE cgConcat #-}
cgConcat :: CellsGroup -> CellsGroup -> CellsGroup
cgConcat cg1 cg2 =
    let c1 = cgColorCells cg1 in
    let b1  = cgBLockCells cg1 in
    let c2 = cgColorCells cg2 in
    let b2  = cgBLockCells cg2 in
    CellsGroup (c1 S.>< c2) (b1 S.>< b2)

{-# INLINE cgCountColors #-}
cgCountColors :: CellsGroup -> Int
cgCountColors cellsGroup = S.length $ cgColorCells cellsGroup

cgAllCells :: CellsGroup -> S.Seq (Pos, Cell)
cgAllCells cg =
    let colors = cgColorCells cg in
    let blocks = cgBLockCells cg in
    colors S.>< blocks

{-# INLINE cgRemoveDuplicatedBlocks #-}
cgRemoveDuplicatedBlocks :: CellsGroup -> CellsGroup
cgRemoveDuplicatedBlocks cg =
    let colors = cgColorCells cg in
    let blocks = cgBLockCells cg in
    CellsGroup colors (F.foldl collectUniqueBlocks S.empty blocks)
    where
        collectUniqueBlocks accu block =
            if (F.elem block accu) then
                accu
            else
                block S.<| accu



-- parsing utilities

readCell :: Char -> Int8
readCell '.' = -1
readCell c   = read [c]

readRow :: String -> [Int8]
readRow row = map readCell row



getBestMaxmaxCleverEval :: GameState -> Int -> (Int, (Move,GameState))
getBestMaxmaxCleverEval gs depth =
    let children = findChildren gs in
    let scoredChildren = map scoreGameState children in
    maximumBy (\(s1,_) (s2,_) -> compare s1 s2) scoredChildren
    where
        scoreGameState :: (Move, GameState) -> (Int, (Move,GameState))
        scoreGameState (m, gs) = (evaluateCleverEval gs (depth - 1), (m,gs))

maxmaxCleverEval :: Int -> GameState -> Int
maxmaxCleverEval 0 gs = staticEvalGS gs
maxmaxCleverEval n gs =
    let nextStates = fmap snd (findChildren gs) in
    let orderedNextStates = sortBy (\s2 s1 -> compare (staticEvalGS s1) (staticEvalGS s2)) nextStates in --sort inversé
    let bestNextStates = take 5 (orderedNextStates) in
    maximum (map (maxmaxCleverEval (n-1)) bestNextStates)

evaluateCleverEval :: GameState -> Int -> Int
evaluateCleverEval gs depth = maxmaxCleverEval depth gs

staticEvalGS :: GameState -> Int
staticEvalGS gs = (gsScore gs) + (gsStaticScore gs) -- + (staticEval $ gsBoard gs)

-- staticEval :: Board -> Int
-- staticEval board = staticEvalGroups (findGroupFastEscapeST board 2)

staticEvalGroups :: [CellsGroup] -> Int
staticEvalGroups cg =
    foldl f 0 cg
    where
        f score cellGroup = score + (cgCountColors cellGroup)*2

getBestMaxmax :: GameState -> Int -> (Int, (Move,GameState))
getBestMaxmax gs depth =
    let children = findChildren gs in
    let scoredChildren = map scoreGameState children in
    maximumBy (\(s1,_) (s2,_) -> compare s1 s2) scoredChildren
    where
        scoreGameState :: (Move, GameState) -> (Int, (Move,GameState))
        scoreGameState (m, gs) = (evaluate gs (depth - 1), (m,gs))

maxmax :: Int -> GameState -> Int
maxmax 0 gs = gsScore gs
maxmax n gs =
    let nextStates = fmap snd (findChildren gs) in
    maximum (map (maxmax (n-1)) nextStates)

evaluate :: GameState -> Int -> Int
evaluate gs depth = maxmax depth gs

findChildren :: GameState -> [(Move, GameState)]
findChildren gs =
    F.toList $ fmap (playGSMove gs) (generateMoves gs)
    where
        playGSMove :: GameState -> Move -> (Move, GameState)
        playGSMove gs move =
            let (board, score, staticEvalScore) = playMove (gsBoard gs) move in
            (move, GameState board (tail (gsNextPuyos gs)) ((gsScore gs) + score) staticEvalScore)

isValidMove :: Move -> Bool
isValidMove (Move (p1,_) (p2,_) _ _) = (isValidPosTuple p1) && (isValidPosTuple p2)

-- | Génère tous les coups différents que l'on peut obtenir avec un Puyo (couple de couleur)
-- | Un puyo peut être joué de 4 façons différentes maximum (4 orientations lorsqu'il a deux couleurs différentes)
generateMovesForOnePuyo :: Puyo -> Int8 -> S.Seq Move
generateMovesForOnePuyo (c1, c2) colIdx =
    let posRef = (colIdx, 11) in
    S.filter isValidMove $ S.fromList [ --TODO optim lorsque deux puyos de même couleurs
        Move { mvFstPuyo = (posRef, c1), mvSndPuyo = (rightP posRef, c2), mvRot = 0, mvCol = colIdx}, -- 0 in the response
        Move { mvFstPuyo = (posRef, c2), mvSndPuyo = (botP posRef, c1), mvRot = 1, mvCol = colIdx}, -- 1 in the response
        Move { mvFstPuyo = (posRef, c1), mvSndPuyo = (leftP posRef, c2), mvRot = 2, mvCol = colIdx}, -- 2 in the response
        Move { mvFstPuyo = (posRef, c1), mvSndPuyo = (botP posRef, c2), mvRot = 3, mvCol = colIdx}  -- 3 in the response
    ] --on ne génère qu'une orientation pour l'instant

generateMoves :: GameState -> S.Seq Move
generateMoves gs
    | null (gsNextPuyos gs) = S.empty
    | otherwise             = toMoves $ head (gsNextPuyos gs)
    where
        toMoves puyo = foldr accuMove S.empty [0..5]
        --one orientation for now
            where
            accuMove colIdx moves = (generateMovesForOnePuyo puyo colIdx) S.>< moves

evalLine :: [Int8] -> Int
evalLine line =
    let (_, _, score) = foldl (f) ((-1), 0, 0) line in
    score
    where
        f (currentQueueColor, currentQueueLength, totalScore) color =
            if (currentQueueColor == color) then
                let lengthQueue = currentQueueLength + 1 in
                (currentQueueColor, lengthQueue, totalScore + (min (lengthQueue - 1) 0))
            else
                (color, 1, totalScore)

    -- let horizontalScore = foldl collectLinesScores 0 [0..11]
    -- where
    --     collectLinesScores score line = score + (evalLine line)


-- position data/functions


-- cell data/functions

leftP, rightP, topP, botP :: Pos -> Pos
leftP  (x,y) = ((x-1), y)
rightP (x,y) = ((x+1), y)
topP   (x,y) = (x, (y+1))
botP   (x,y) = (x, (y-1))

left, right, top, bot :: Board -> Pos -> (Pos, Cell)
left  b p = (leftP  p, getValidCellPos b (leftP p))
right b p = (rightP p, getValidCellPos b (rightP p))
top   b p = (topP   p, getValidCellPos b (topP p))
bot   b p = (botP   p, getValidCellPos b (botP p))

getValidCellPos :: Board -> Pos -> Cell
getValidCellPos b (x, y) =
    if (isValidPos x y) then
        getCell b x y
    else
        invalidCell

isValidCell :: Cell -> Bool
isValidCell (-2)    = False
isValidCell _       = True

{-# INLINE isValidPosTuple #-}
isValidPosTuple :: Pos -> Bool
isValidPosTuple (x,y) = isValidPos x y

{-# INLINE isValidPos #-}
isValidPos :: Int8 -> Int8 -> Bool
isValidPos x y = x >= 0 && x <= 5 && y >= 0 && y <= 11

{-# INLINE isEmptyCell #-}
isEmptyCell :: Cell -> Bool
isEmptyCell color = color == -1

isOccupiedCell :: Cell -> Bool
isOccupiedCell color = color > -1

isBlockCell :: Cell -> Bool
isBlockCell color = color == 0

isColorCell :: Cell -> Bool
isColorCell color = isColor color

isColor :: Int8 -> Bool
isColor c = c > 0

removeInvalidCells :: [(Pos, Cell)] -> [(Pos, Cell)]
removeInvalidCells = filter (isValidCell . snd)

getNeighboursPos :: Pos -> [Pos]
getNeighboursPos pos = [leftP pos, rightP pos, topP pos, botP pos]

getNeighbours :: Board -> Pos -> [(Pos, Cell)]
getNeighbours b pos = [left b pos, right b pos, top b pos, bot b pos]

dirFast :: MBoard s -> Pos -> (Pos -> Pos) -> ST s (Pos, Cell)
dirFast mBoard pos moveFunc = do
    let newPos = moveFunc pos
    cell <- do
         if (isValidPosTuple newPos) then
             ST.readArray mBoard newPos
         else
             return invalidCell
    return (newPos, cell)

getNeighboursFast :: MBoard s -> Pos -> ST s [(Pos, Cell)]
getNeighboursFast mb pos =
    do
        dl <- dirFast mb pos leftP
        dr <- dirFast mb pos rightP
        dt <- dirFast mb pos topP
        db <- dirFast mb pos botP
        return [dl, dr, dt, db]

getValidNeighbours :: Board -> Pos -> [(Pos, Cell)]
getValidNeighbours b pos = removeInvalidCells (getNeighbours b pos)



-- board data/functions

getCellTuple :: Board -> Pos -> Cell
getCellTuple board (colIdx, rowIdx) = getCell board colIdx rowIdx

getCell :: Board -> Int8 -> Int8 -> Cell
getCell (Board board) colIdx rowIdx =
   board A.! (colIdx, rowIdx)

setCell :: Board -> Pos -> Cell -> Board
setCell b pos (-2) = b
setCell (Board board) pos cell = Board (board A.// [(pos, cell)])

getCells :: Board -> [Cell]
getCells (Board b) = A.elems b

getColumnCells :: Board -> Int8 -> [Cell]
getColumnCells board colIdx =
    map (getCell board colIdx) [0..11]


setCells :: Board -> [(Pos,Cell)] -> Board
setCells board [] = board
setCells board posCells = foldl (\b (pos,cell) -> setCell b pos cell) board posCells

setEmptyCell :: Board -> Pos -> Board
setEmptyCell (Board board) pos = Board (board A.// [(pos, ((-1) ))])

setEmptyCells :: Board -> [Pos] -> Board
setEmptyCells board poss =
    setCells board (map toEmptyCell poss)
    where toEmptyCell pos = (pos, (-1))

--retourne un tuple (indice, couleur)
getColHeigth :: Board -> Int8 -> (Int8, Cell)
getColHeigth board colIdx =
    f (0,-1) [0..11]
    where
        f (h,c) []     = (h, c)
        f (h,c) (y:ys) =
            let col = (getCell board colIdx y) in
            if (col == -1) then (h,c) else f (h+1, col) ys



getRowMinHeightIdx :: Board -> Int8
getRowMinHeightIdx board =
    f (-1) 13 [0..5]
    where
        f minColIdx minHeigth []     = minColIdx
        f minColIdx minHeigth (idx:xs) =
            let currentHeigth = fst (getColHeigth board idx) in
            if (currentHeigth < minHeigth) then
                f idx currentHeigth xs
            else
                f minColIdx minHeigth xs

getRowSameColorIdx :: Board -> Cell -> Int8
getRowSameColorIdx board color =
    f [0..5]
    where
        f []       = -1
        f (idx:xs) =
            let currentColor = snd (getColHeigth board idx) in
            if (currentColor == color) then
                idx
            else
                f xs

type VisitedCells s = STUArray s (Int8,Int8) Bool
type MBoard s = STUArray s (Int8,Int8) Int8

-- {-# INLINE getNoneEmptyNeighbours #-}
getNoneEmptyNeighbours :: MBoard s -> Pos -> ST s [(Pos, Cell)]
getNoneEmptyNeighbours mBoard pos =
    do
        !neigh <- getNeighboursFast mBoard pos
        return (filter (\x -> (isOccupiedCell $ snd $ x) && (isValidCell $ snd $ x)) (neigh))

-- {-# INLINE visitNeighbours #-}
visitNeighbours :: MBoard s -> Cell -> Pos -> (VisitedCells s) -> ST s (CellsGroup)
visitNeighbours mBoard colorRef pos visitedCells =
    do
        !cellsToVisit <- getNoneEmptyNeighbours mBoard pos
        !subGroups <- mapM (\pc -> visitPos mBoard colorRef pc visitedCells) cellsToVisit
        return (foldl cgConcat cgEmpty subGroups)

--visite une position
{-# INLINE visitPos #-}
visitPos :: MBoard s -> Cell -> (Pos,Cell) -> (VisitedCells s) -> ST s (CellsGroup)
visitPos mBoard colorRef (pos,currentColor) visitedCells =
    do
        visited <- ST.readArray visitedCells pos

        --let currentColor = board A.! pos
         --4 cas :
        -- -> si la cell est déjà visitée et qu'il s'agit d'un block,
        --           > on l'ajoute quand même car un block peut faire partie de plusieurs groupes
        -- -> si la cell est déjà visitée et qu'il ne s'agit pas d'un block,
        --           > on return direct
        -- -> si la cell n'est pas visitée et qu'elle est de la bonne couleur
        --           > on l'ajoute aux celle collectée
        --           > on la marque visitée
        --           > on visite toutes ses voisines

        if (isEmptyCell currentColor) then do
            ST.writeArray visitedCells pos True
            return (cgEmpty)
        else
            if (visited) then
                if (isBlockCell currentColor) then
                    return (cgCreateWithOneBlock (pos, currentColor))  --((pos, currentColor) S.<| S.empty)
                else
                    return (cgEmpty)
            else do
                if (isBlockCell currentColor) then
                    return (cgCreateWithOneBlock (pos, currentColor))
                else
                    if (currentColor == colorRef) then do
                        ST.writeArray visitedCells pos True
                        --trace ("visit pos : " ++ show (pos) ++ "color " ++ (show currentColor)) (visitNeighbours b colorRef pos visitedCells)
                        neigh <- (visitNeighbours mBoard colorRef pos visitedCells)
                        return (cgAddColorCell neigh pos currentColor)
                    else
                        return (cgEmpty)


findGroupFast :: MBoard s -> Pos -> VisitedCells s -> ST s (CellsGroup)
findGroupFast mBoard currentPos visitedCells =
    do
        currentColor <- ST.readArray mBoard currentPos
        !cellsGroup <- visitPos mBoard currentColor (currentPos, currentColor) visitedCells
        --les cellules block peuvent apparaitre plrs fois dans un groupe
        return (cgRemoveDuplicatedBlocks cellsGroup)


findGroupFastEscapeST :: Board -> [Pos] -> Int -> [CellsGroup]
findGroupFastEscapeST (Board board) cellsToVisit minLength =
    runST $ do
        mBoard <- ST.thaw board
        !res <- findGroupsFast mBoard cellsToVisit -- (A.range ((0,0),(5,11)))
        return (filter (( >= minLength) . cgCountColors) res)

-- retourne tous les groupes de taille >= 2
findGroupsFast :: MBoard s -> [Pos] -> ST s [CellsGroup]
findGroupsFast mBoard cellsToCheck =
    do
        visitedCells <- ST.newArray ((0,0),(5,11)) False :: ST s (VisitedCells s)
        -- let (x_lo, y_lo) = fst $ A.bounds $ board
        -- let (x_up, y_up) = snd $ A.bounds $ board
        --mBoard <- ST.thaw board
        -- forM_ [x_lo..x_up] $ \x -> do
        --     forM_ [y_lo..y_up] $ \y -> do
        --         res <- findGroupFast board (x,y) visitedArr
        !res <- foldM (fg mBoard visitedCells) [] cellsToCheck
        return (filter (( >= 2) . cgCountColors) res)
        where
            fg mb visited accu pos@(x,y) = do
                v <- ST.readArray visited (x,y)
                if (v) then
                    return (accu)
                else do
                    group <- (findGroupFast mb pos visited)
                    return (group:accu)

--return a group of same color cells, visited Cells
-- findGroup :: Board -> (M.Map Pos Pos) -> Pos -> ([(Pos, Cell)], (M.Map Pos Pos))
-- findGroup board visitedCells pos =
--     let color = (getCellTuple board pos) in
--     let (rcells, rvisited) = f visitedCells pos in
--     (nub (F.foldr (\e accu -> e:accu) [] rcells),  rvisited)
--     where
--         --   couleur de référence -> cellules déjà visitées -> cellule courante à visiter
--         --   -> (cellules formant le groupe courant, cellules déjà visitées)
--         f :: (M.Map Pos Pos) -> Pos -> (S.Seq (Pos, Cell), (M.Map Pos Pos))
--         f visitedCells' currentPos =
--             --une cell de type block n'a pas de voisins
--             let currentCell = getCellTuple board currentPos in
--             let colorRef = currentCell in
--             let neighbourds = (if (isBlockCell currentCell) then []  else getValidNeighbours board currentPos) :: [(Pos, Cell)]  in
--             let sameColorNeighbours = filter ((== colorRef) . snd) neighbourds in
--             let coloredCellsToVisit = map fst $ filter (\(p,c) -> M.notMember p visitedCells') sameColorNeighbours in
--             --on visite les cells block même si elles ont déjà été visitées car une même cell block peut appartenir à plusieurs groupes (car chaque groupe, lorsqu'il sera détruit, l'entrainera avec lui)
--             let blockCellsToVisit = map fst (filter (isBlockCell . snd) neighbourds) in
--             let newVisistedCells =  M.insert currentPos currentPos visitedCells' in
--             --si c'est une cellule colorée, on la viste. S'il s'agit d'une cellule block, on la visite aussi (mais elle aura 0 voisins)
--             if ((currentCell) == colorRef || (isBlockCell currentCell)) then
--                 let (resVColored, resVVisited) = (visit (coloredCellsToVisit ++ blockCellsToVisit) newVisistedCells) in
--                 ((currentPos, currentCell) S.<| resVColored, resVVisited) --tuple retournant : les cells du groupe et les cells visitées
--             else
--                 visit coloredCellsToVisit newVisistedCells
--             where
--                 visit :: [Pos] -> (M.Map Pos Pos) -> (S.Seq (Pos, Cell), (M.Map Pos Pos)) --return tuple (cells de même couleur, cells visitées)
--                 visit poss visited = foldl accuF (S.empty, visited) poss
--                     where
--                         accuF :: (S.Seq (Pos, Cell), (M.Map Pos Pos)) -> Pos -> (S.Seq (Pos, Cell), (M.Map Pos Pos))
--                         accuF (coloredaccu, visitedaccu) pos =
--                             let (resVColored, resVVisited) = f visitedaccu pos in
--                             (coloredaccu S.>< resVColored, (M.unions [visitedaccu, resVVisited]))


-- findGroups :: Board -> [[(Pos, Cell)]]
-- findGroups board =
--     Data.List.filter ((>3) . countColors) (f (A.range ((0,0), (5,11))) M.empty)
--     where
--         f :: [Pos] -> (M.Map Pos Pos) -> [[(Pos, Cell)]]
--         f [] _     = []
--         f (p:ps) visitedCells =
--             let currentCell = getCellTuple board p in
--             if (shouldVisitCell p currentCell) then
--                 let (group, visited) = findGroup board visitedCells p in
--                 group : (f ps visited)
--             else
--                 f ps visitedCells
--             where
--                 shouldVisitCell pos cell = isColorCell cell && (M.notMember pos visitedCells)

countColors :: [(Pos, Cell)] -> Int
countColors cells =
    foldl (\accu c -> if (isColorCell $ snd c) then accu + 1 else accu) 0 cells

destroyGroups :: Board -> [[(Pos, Cell)]] -> Board
destroyGroups board groups = setEmptyCells board (map fst (concat groups))

destroyGroupsFast :: MBoard s -> [[(Pos, Cell)]] -> ST s ()
destroyGroupsFast mBoard groups = do
    let allPos = map fst (concat groups)
    forM_ allPos $ \(x,y) -> do
        ST.writeArray mBoard (x, y) (-1)


moveToCells :: Move -> [(Pos, Cell)]
moveToCells (Move (p1, c1) (p2, c2) _ _) = [(p1, c1), (p2, c2) ]

intsColToCells :: Int8 -> [Cell] -> [(Pos, Cell)]
intsColToCells colIdx colors =
    f colors 0
    where
        f [] _ = []
        f (c:cs) rowIdx = ((colIdx, rowIdx),(c)) : f cs (rowIdx + 1)

-- fallColumn :: [Cell] -> [Int]
-- fallColumn []  = []
-- fallColumn col =
--     f (reverse col) [] []
--     where
--         f [] nums empties     = nums ++ empties
--         f (x:xs) nums empties =
--             case x of
--                 -1 -> f xs nums ((-1):empties)
--                 n  -> f xs (n:nums) empties

-- fallOneCol :: Board -> Int -> Board
-- fallOneCol board colIdx =
--     let posCells = intsColToCells colIdx ( fallColumn (getColumnCells board colIdx)) in
--     setCells board posCells

-- fallPuyos :: Board -> [Int] -> Board
-- fallPuyos board colToFall=
--     foldl (\accu colIdx -> fallOneCol accu colIdx) board colToFall

fallOneColFast :: MBoard s -> Int8 -> ST s (Pos)
fallOneColFast mBoard colIdx =
    do
        topOfTheColRef <- newSTRef (-1)
        lowestEmptyCellIdxRef <- newSTRef (-1)
        foundEmptyCellRef <- newSTRef False
        forM_ [0..11] $ \y -> do
            currentCell <- ST.readArray mBoard (colIdx,y)
            foundEmptyCell <- readSTRef foundEmptyCellRef
            if (not foundEmptyCell) then
                if (isEmptyCell currentCell) then do
                    modifySTRef foundEmptyCellRef (const True)
                    modifySTRef lowestEmptyCellIdxRef (const y)
                else
                    --modifySTRef topOfTheCol (const y)
                    return ()
            else
                if (isOccupiedCell currentCell) then do
                    lowestEmptyCellIdx <- readSTRef lowestEmptyCellIdxRef
                    --on efface la cellule d'où elle était
                    ST.writeArray mBoard (colIdx, y) (-1)
                    --pour la mettre là où elle est tombée
                    ST.writeArray mBoard (colIdx, lowestEmptyCellIdx) currentCell
                    modifySTRef lowestEmptyCellIdxRef (+1)
                    modifySTRef topOfTheColRef (const lowestEmptyCellIdx)
                else
                    return ()

        topOfTheCol <- readSTRef topOfTheColRef
        return (colIdx, topOfTheCol)


fallPuyosFast :: MBoard s -> [Cell] -> ST s ([Pos])
fallPuyosFast mBoard colToFall =
    do
        foldM collectTops [] colToFall
        where
            collectTops accu colIdx =
                do
                    !top <- fallOneColFast mBoard colIdx
                    return (top:accu)

-- addSurroundingCells :: [Pos] -> [Pos]
-- addSurroundingCells poss =
--     let surrounding = concat $ map (\e -> filter (isValidPosTuple) (getNeighboursPos e)) poss in
--     nub (poss ++ surrounding)

--retourne le nouveau plateau ainsi que deux scores : celui du aux combos, et celui de l'évaluation statique du nouveau board
executeGravityLoopFast :: Board -> Move -> (Board, Int, Int)
executeGravityLoopFast (Board board) move =
    runST $ do
        mBoard <- ST.thaw board
        (score, staticEvalScore) <- f mBoard 0 0 True
        freezed <- freeze mBoard
        return (Board freezed, score, staticEvalScore)
        where
            f :: MBoard s -> Int -> Int -> Bool -> ST s (Int, Int)
            f mb score comboLevel firstCall = do
                -- lorsque c'est le premier appel, il s'agit de faire tomber uniquement un coup
                -- on considère que le reste est déjà tombé, donc on ne fait tomber que les colonnes du coup
                -- de même on analyse que les groupes formés à partir des nouveaux puyos tombés
                let columnsToFall = if (firstCall) then (nub [(px $ fst $ mvFstPuyo $ move),(px $ fst $ mvSndPuyo $ move)]) else [0..5]
                fallPoss <- fallPuyosFast mb columnsToFall
                if (length columnsToFall == 1) then
                    --hack dégueux pour, si jamais une seule colonne est tombée, on n'a retourné qu'une seule pos (celle tout en haut
                    --on rajoute la pos juste en dessous qui correspond au deuxième puyo en dessu du top
                    let (x,y) = head fallPoss in
                    f2 ((x, y - 1):fallPoss)
                else
                    f2 (fallPoss)

                where
                    f2 fp = do
                        let cellsToCheckForGroup = if (False) then (filter (isValidPosTuple) fp) else A.range ((0,0),(5,11)) -- addSurroundingCells [(x,y) | x <- columnsToFall, y <- [0..11]]
                        -- groupes de 2 couleurs min
                        groups <- findGroupsFast mb cellsToCheckForGroup
                        -- groupes de 4 couleurs min
                        let groups4min = (filter (( >= 4) . cgCountColors) groups)
                        let diffColors = Data.List.length groups4min
                        let localScore = sum (map cgCountColors groups4min)
                        let totalScore = localScore + if (comboLevel > 0) then 10 ^ comboLevel else 0
                        let staticEvalScore = staticEvalGroups groups
                        destroyGroupsFast mb (map (F.toList . cgAllCells) groups4min)
                        if (localScore == 0) then do
                            let staticEvalScore = staticEvalGroups groups
                            return (score, staticEvalScore)
                        else
                            f mb (score+totalScore) (comboLevel + 1) False



-- executeGravityLoop :: Board -> Move -> (Board, Int)
-- executeGravityLoop board move =
--     f board 0 0 True
--     where
--         f :: Board -> Int -> Int -> Bool -> (Board, Int)
--         f b score comboLevel fallOnlyMoveCol =
--             let columnsToFall = if (fallOnlyMoveCol) then (nub [(px $ fst $ mvFstPuyo $ move),(px $ fst $ mvSndPuyo $ move)]) else [0..5] in
--             let allDown = fallPuyosFast b columnsToFall in
--             let groups = findGroupsFast allDown in
--             let diffColors = Data.List.length groups in
--             let localScore = sum (map cgCountColors groups) in
--             let totalScore = localScore + if (comboLevel > 0) then 10 ^ comboLevel else 0 in -- + (10 * (diffColors-1)) + (10 ^ comboLevel)
--             let destroyedPuyos = destroyGroups allDown (map (F.toList . cgAllCells) groups) in
--             if (localScore == 0) then
--                 (allDown, score)
--             else
--                 f destroyedPuyos (score+totalScore) (comboLevel + 1) False
--                 --on ne se limite pas aux colonnes du coup pour tout faire tomber car les destructions de group ont pu intéragir sur toutes les colonnes
--                 --(TODO : essayer de déterminer les colonnes touchés par les destructions de groupe pour ne faire tomber que celles là)

canSetMoveCells :: Board -> [(Pos, Cell)] -> Bool
canSetMoveCells board poss =
    all ( isEmptyCell . (getCellTuple board) . fst) poss

setMove :: Board -> Move -> Maybe Board
setMove board move =
    let moveCells = moveToCells move in
    if (canSetMoveCells board moveCells) then
        Just (setCells board moveCells)
    else
        Nothing

playMove :: Board -> Move -> (Board, Int, Int)
playMove board move =
    case setMove board move of
        Just puyosSetted -> executeGravityLoopFast puyosSetted move
        Nothing          -> (board, -9999, -9999) -- si on ne peut pas jouer alors la partie est considérée comme perdue

-- getBestColumn :: Board -> Puyo -> (Int, Int)
-- getBestColumn board puyo =
--     maximumBy compareTuples (map mapToTuple [0..5])
--     where
--         mapToTuple :: Int -> (Int, Int)
--         mapToTuple colIdx = (colIdx, snd (playMove board (Move puyo colIdx 1)))
--         compareTuples t1 t2 = compare (snd t1) (snd t2)

sexyPrintBoards :: S.Seq (Maybe Board) -> String
sexyPrintBoards bs = F.foldr (f) "" bs
    where f Nothing accu = accu
          f (Just b) accu = (sexyPrint b) ++ accu


sexyPrint :: Board -> String
sexyPrint b =
    (foldl joinLines [] (toPrettyIntsArray b)) ++ "\n"
    where
        joinLines l ls = ls ++ "\n" ++ l
        toPrettyIntsArray board = [ concat (sexyRow [getCell board x y | x <- [0..5]]) | y <- [0..11] ]
        sexyRow row = map sexyCell row
        sexyCell cell = sexyDigit (cell)
        sexyDigit :: Cell -> String
        sexyDigit d
            | d == (-1)  = " ."
            | otherwise  = " " ++ [intToDigit (fromIntegral d)]

sexyPrintGameState :: GameState -> String
sexyPrintGameState (GameState board ([]) score staticScore) =
    "next puyo : nothing" ++
    "score : " ++ (show score) ++ "\n" ++
    "staticScore : " ++ (show staticScore) ++ "\n" ++
    (sexyPrint board)
sexyPrintGameState (GameState board (p:ps) score staticScore) =
    "next puyo : " ++ (show p) ++ "\n" ++
    "score : " ++ (show score) ++ "\n" ++
    "staticScore : " ++ (show staticScore) ++ "\n" ++
    (sexyPrint board)

sexyPrintGameStates :: [GameState] -> String
sexyPrintGameStates gss =
    foldl (++) "" (map sexyPrintGameState gss)

printlines :: [String] -> String
printlines ls =
    foldl (\accu l -> l ++ "\n" ++ accu) "" ls

loadBoardFromInts :: [[Cell]] -> Board
loadBoardFromInts ints =
    let revInts = ints in
    Board ( A.array ((0,0),(5,11)) [((x, y), (revInts!!(fromIntegral y)!!(fromIntegral x))) | y <- [0..11], x <- [0..5]] )

loadBoard :: String -> Board
loadBoard s =
    let intBoard = reverse $ map (readRow . T.unpack) (T.split (=='\n') (T.pack s)) in
    loadBoardFromInts intBoard

printScoredGameState :: (Move, GameState) -> String
printScoredGameState (m,gs) =
    "move : " ++ (printMove m) ++ (sexyPrintGameState gs)

printScoredGameStates :: [(Move, GameState)] -> String
printScoredGameStates xs =
    foldl (++) "" (map printScoredGameState xs)

loop :: IO ()
loop = do
    --parsing
    incomingColors <- replicateM 8 $ do
        input_line <- getLine
        let input = words input_line
        let colora = read (input!!0) :: Int8 -- color of the first block
        let colorb = read (input!!1) :: Int8 -- color of the attached block
        return (colora, colorb)

    -- linesBoard <- replicateM 12 $ do
    --     input_line <- getLine
    --     let row = input_line :: String
    --     return (row)

    intBoard <- replicateM 12 $ do
        input_line <- getLine
        let row = input_line :: String
        let boardRow = readRow row
        return (boardRow)


    hPutStrLn stderr "--- int board"
    hPutStrLn stderr $ show $ intBoard --printlines $ linesBoard
    hPutStrLn stderr "---"

    let revIntBoard = reverse intBoard

    --let myBoard = Board (M.fromList [((x,y), Cell (revIntBoard!!y!!x) (x,y)) | y <- [0..11], x <- [0..5]])

    let myBoard = Board ( A.array ((0,0),(5,11)) [((x,y), (revIntBoard!!(fromIntegral y)!!(fromIntegral x))) | y <- [0..11], x <- [0..5]] )

    intBoard2 <- replicateM 12 $ do
        input_line <- getLine
        let row = input_line :: String -- One line of the map ('.' = empty, '0' = skull block, '1' to '5' = colored block)
        let boardRow = readRow row
        return (boardRow)

    -- algo

    --let enemyBoard = Board $ reverse intBoard2
    hPutStrLn stderr "--- my board"
    --hPutStrLn stderr $ show myBoard
    hPutStrLn stderr "---"

    let gameState = GameState myBoard incomingColors 0 0

    --let sameColorColumn = getRowSameColorIdx myBoard (fst(incomingColors!!0))
    --let bestDirectPointsColumnPoints = getBestColumn myBoard (incomingColors!!0)
    let (score, (moveMaxmax, state)) = getBestMoveMCTS gameState --getBestMaxmaxCleverEval gameState 3
    let lowestColumn = getRowMinHeightIdx myBoard
    randomColumn <- randomRIO (0 :: Int8, 5)

    -- le nouveau score doit être au moins > à 4
    let move = if ((trace ("score : " ++ (show score) ++ " ") score) < 0) then defaultMove $ lowestColumn else moveMaxmax

    -- "x": the column in which to drop your blocks
    putStrLn $ printMove move

    loop

defaultMove :: Int8 -> Move
defaultMove colIdx = Move ((0,0),0) ((0,0),0) 1 colIdx

printMove :: Move -> String
printMove (Move _ _ rot col) = (show col) ++ " " ++ (show rot)

puyos = (2,2) :: (Int,Int)

--choisit bien le plus gros groupe
tab1 = "\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\.....1\n\
\1....1\n\
\1.1..1"

board1 = loadBoard tab1
moves1 = [(1,1),(5,5),(5,5),(5,5)]
gameState1 = GameState board1 moves1 0 0
generatedMoves = generateMoves gameState1
children1 = findChildren gameState1
(score1, result1) = getBestMaxmax gameState1 1
bestMove1 = fst result1
--putStrLn $ sexyPrint $ fst $ playMove board1 (Move {mvFstPuyo = ((0,11),1), mvSndPuyo = ((1,11),1), mvRot = 0, mvCol = 0})

--choisit bien le groupe qui déclenche le combo
tab2 = "\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\.....2\n\
\.....2\n\
\1....2\n\
\1....1\n\
\1...12"

board2 = loadBoard tab2
moves2 = [(1,1),(5,5),(5,5),(5,5)]
gameState2 = GameState board2 moves2 0 0
generatedMoves2 = generateMoves gameState2
children2 = findChildren gameState2
(score2, result2) = getBestMaxmax gameState2 1
bestMove2 = fst result2

--choisit de construire un combo
tab3 = "\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\.....2\n\
\.....2\n\
\1....2\n\
\1....1\n\
\1....2"

board3 = loadBoard tab3
moves3 = [(1,1),(1,1),(5,5),(5,5)]
gameState3 = GameState board3 moves3 0 0
generatedMoves3 = generateMoves gameState3
children3 = findChildren gameState3
(score3, result3) = getBestMaxmax gameState3 2
bestMove3 = fst result3
--putStrLn $ sexyPrint $ fst (executeGravityLoopFast board3 (Move {mvFstPuyo = ((4,11),3), mvSndPuyo = ((4,11),3), mvRot = 0, mvCol = 0}))

-- situation compliquées
tab4 = "\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\.....3\n\
\.2...3\n\
\11...2\n\
\55...2\n\
\15...2\n\
\124311\n\
\154332"

board4 = loadBoard tab4
moves4 = [(1,2),(2,1),(1,5),(4,3)]
gameState4 = GameState board4 moves4 0 0
generatedMoves4 = generateMoves gameState4
children4 = findChildren gameState4
(score4, result4) = getBestMaxmax gameState4 3
bestMove4 = fst result4

-- groupes à trouver
tab5 = "\
\.....5\n\
\.11...\n\
\.11..1\n\
\.....1\n\
\.....1\n\
\......\n\
\......\n\
\.333..\n\
\.3.3.2\n\
\.333.2\n\
\.....2\n\
\1111.2"

board5 = loadBoard tab5

--fait bien péter les block
tab6 = "\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\....1.\n\
\....1.\n\
\....1.\n\
\..2.2.\n\
\..022.\n\
\..1101"

board6 = loadBoard tab6
moves6 = [(1,1),(5,5),(5,5),(5,5)]
gameState6 = GameState board6 moves6 0 0
generatedMoves6 = generateMoves gameState6
children6 = findChildren gameState6
(score6, result6) = getBestMaxmax gameState6 1
bestMove6 = fst result6


--choisit de construire un combo compliqué
tab7 = "\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\.....5\n\
\....55"

board7 = loadBoard tab7
moves7 = [(1,1), (1,5),(1,5),(5,5),(5,5)]
gameState7 = GameState board7 moves7 0 0
generatedMoves7 = generateMoves gameState7
children7 = findChildren gameState7
(score7, result7) = getBestMaxmax gameState7 3
bestMove7 = fst result7

--groupes qui se touchent
tab8 = "\
\......\n\
\......\n\
\.1111.\n\
\.1221.\n\
\.1221.\n\
\.1111.\n\
\......\n\
\......\n\
\.0....\n\
\.0.333\n\
\.11355\n\
\.11355"

board8 = loadBoard tab8
moves8 = [(1,1), (1,5),(1,5),(5,5),(5,5)]
gameState8 = GameState board8 moves8 0 0
generatedMoves8 = generateMoves gameState8
children8 = findChildren gameState8
(score8, result8) = getBestMaxmax gameState8 3
bestMove8 = fst result8

--groupes qui se touchent
tab9 = "\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\121212\n\
\121212"

tab92 = "\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\......\n\
\212121\n\
\121212"

board9 = loadBoard tab9
board92 = loadBoard tab92
moves9 = [(1,1), (1,5),(1,5),(5,5),(5,5)]
gameState9 = GameState board9 moves9 0 0
generatedMoves9 = generateMoves gameState9
children9 = findChildren gameState9
(score9, result9) = getBestMaxmax gameState9 3
bestMove9 = fst result9

pb1 = read "[[-1,3,5,5,1,4],[-1,4,4,3,1,4],[-1,3,2,3,1,4],[-1,4,4,3,2,2],[-1,1,1,0,2,4],[-1,0,1,0,0,4],[1,0,0,0,0,0],[1,0,0,2,0,0],[0,0,0,5,5,0],[0,5,0,4,4,5],[0,5,4,1,4,5],[0,5,4,2,2,4]]" :: [[Int8]]
boardpb1 = loadBoardFromInts (reverse pb1)
movespb1 = [(1,2),(5,5),(5,2),(3,3)]
gameStatepb1 = GameState boardpb1 movespb1 0 0
generatedMovespb1 = generateMoves gameStatepb1
childrenpb1 = findChildren gameStatepb1
(scorepb1, resultpb1) = getBestMaxmax gameStatepb1 3
bestMovepb1 = fst resultpb1
